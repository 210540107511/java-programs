import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 */

/**
 * @author Brijesh Jagad
 *
 */
public class StringArrayList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ArrayList<String> city =  new ArrayList<>();

		city.add("Delhi");
		city.add("Mumbai");
		city.add("Banglore");
		city.add("Hyderabad");
		city.add("Ahmedabad");
		
		System.out.println("Data of list =");
		System.out.println(city);
		
		Collections.replaceAll(city, "Ahmedabad", "Surat");
		
		System.out.println("Data of list =");
		System.out.println(city);

	}

}
