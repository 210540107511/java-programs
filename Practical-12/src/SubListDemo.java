import java.util.ArrayList;
import java.util.Scanner;

/**
 * 
 * @author Brijesh Jagad
 *
 */
public class SubListDemo {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		ArrayList<String> names =  new ArrayList<>();
		
		names.add("Aarav");
		names.add("Kabir");
		names.add("Vivaan");
		names.add("Ayaan");
		names.add("Aditya");
		
		System.out.println("All names in list =");
		System.out.println(names);
		
		System.out.println("Names Starts with A :");
		names.forEach(
			e->{
				if(e.charAt(0) == 'A')
					System.out.println(e);
			}
		);
		

	}

}
