import java.util.*;

class ArrayListDemo{
	
	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter 5 numbers:");
		for (int i=0;i<5;i++) {
			list.add(sc.nextInt());	
		}
		
		Collections.sort(list);

		System.out.print("Elements in list:");
		for (Integer i : list) {
			System.out.println(i);
		}
		sc.close();
	}
}