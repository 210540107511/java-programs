import java.util.ArrayList;

/**
 * 
 * @author Brijesh Jagad
 *
 */

class Loan{
	@Override
	public String toString() {
		String msg = "Called Loan Object.";
		return msg;
	}
}

class Date{
	@Override
	public String toString() {
		String msg = "Called Date Object.";
		return msg;
	}
}

class string{
	@Override
	public String toString() {
		String msg = "Called string Object.";
		return msg;
	}
}

class Circle{
	@Override
	public String toString() {
		String msg = "Called Circle Object.";
		return msg;
	}
}

public class ObjectArrayList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		ArrayList<Object> list = new ArrayList<>();
		
		list.add(new Loan());

		list.add(new Date());
		
		list.add(new string());
		
		list.add(new Circle());
		
		list.forEach(e->{
			System.out.println(e);
		});

	}

}
