
class A {
	
	int a = 10;
	static class B{
		int a = 20;
		
		void print() {
			System.out.println("a = "+a);
		}
	}
}

public class InheritanceDemo {
	public static void main(String[] args) {
		A.B ab = new A.B();
		ab.print();
		
	}
}
