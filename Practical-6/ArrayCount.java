
import java.util.Scanner;

class ArrayCount{

	public static void main(String[] args) {
		int n;
		int even_count=0,odd_count=0;
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size:");
		n=sc.nextInt();
		int a[] = new int[n];

		System.out.println("Enter Array Elements:");
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
			if(a[i]%2 == 0){
				even_count++;
			}else{
				odd_count++;
			}
		}

		System.out.println("\nEven Number Count:"+even_count);

		System.out.println("Odd Number Count:"+odd_count);

	}
	
}