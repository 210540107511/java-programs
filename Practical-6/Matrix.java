import java.util.Scanner;

class Matrix{
	public static void main(String[] args) {

		int n=2;
		int m=n;
		int a[][] = new int[n][m];
		Scanner sc = new Scanner(System.in);
	
		System.out.println("Enter Array Elements("+n+"x"+m+"):");

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = sc.nextInt();
			}
		}

		System.out.println("Matrix :");
		for ( int arr[] : a ) {
			for (int num : arr) {
				System.out.print(num+"\t");
			}
			System.out.println();
		}
	}
}