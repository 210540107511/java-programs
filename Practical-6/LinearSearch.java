
import java.util.Scanner;

class LinearSearch{

	public static void main(String[] args) {
		int n;
		
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter Array Size:");
		n=sc.nextInt();
		int a[] = new int[n];

		System.out.println("Enter Array Elements:");
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}

		System.out.print("Enter Number to check whether number is present ot not:");
		int num = sc.nextInt(); 
		int flag = 0,i;
		
		//Linear Search
		for (i = 0; i < n; i++) {
			if(a[i]==num){
				flag=1;
				break;
			}
		}
		if(flag == 1){
			System.out.println("Element found at "+ i +" Index.");
		}else{
			System.out.println("Element not Found. :(");
		}
	}
	
}