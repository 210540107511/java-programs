
import java.util.Scanner;

class BubbleSort{

	private static int[] bubble_sort(int a[]){

		int temp;
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a.length-(i+1); j++) {
				if( a[j] > a[j+1]){
					temp = a[j];
					a[j] = a[j+1];
					a[j+1] = temp;
				}
			}
		}

		return a;
	} 

	public static void main(String[] args) {
		int n = 10;

		Scanner sc = new Scanner(System.in);

		// System.out.print("Enter Array Size:");
		// n=sc.nextInt();
		int a[] = new int[n];

		System.out.println("Enter Array Elements:");
		for (int i = 0; i < n; i++) {
			a[i] = sc.nextInt();
		}

		int sorted[] = bubble_sort(a);

		for (int i=0; i<sorted.length;i++ ) {
			System.out.println("\t"+sorted[i]);
		}
	}
	
}