import java.util.Scanner;

class MatrixMul{
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n/*=2*/;
		System.out.print("Enter size of Matrix:");
		n = sc.nextInt();
		int m=n;
		int a[][] = new int[n][m];
		int b[][] = new int[n][m];
		int c[][] = new int[n][m];

		System.out.println("Enter first Array Elements("+n+"x"+m+"):");

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = sc.nextInt();
			}
		}

		System.out.println("Enter second Array Elements("+n+"x"+m+"):");

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				b[i][j] = sc.nextInt();
			}
		}

		System.out.println("Third (Mul) Matrix :");

		for (int i=0;i<n ;i++) {
			for (int j=0;j<m ;j++) {
				c[i][j] = 0;
				for(int k=0;k<n;k++){
					c[i][j] += a[i][k]*b[k][j];
				}
				System.out.print("\t"+c[i][j]);
			}
			System.out.println();
		}

	}
}