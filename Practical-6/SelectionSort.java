import java.util.Scanner;

class SelectionSort{

	static int[] selection_sort(int[] a){
		
		int min,minIndex;

		for (int i=0; i < a.length; i++) {
			min = a[i];
			minIndex=i;
			for (int j=i+1; j < a.length; j++) {
				if (min > a[j]) {
					min = a[j];
					minIndex = j;
				}
			}//j
			if (minIndex != i) {
				a[minIndex] = a[i];
				a[i] = min;
			}	
		}//i
		return a;
	}

	public static void main(String[] args) {
		int n=5;
		int a[] = new int[n];
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Elements:");
		for (int i=0;i<a.length;i++) {
			a[i] = sc.nextInt();
		}

		int sorted_array[] = selection_sort(a);
		System.out.println("Array Elements:");
		for (int i=0;i<a.length;i++) {
		System.out.print(sorted_array[i]+"\t");
		}
	}
}