import java.util.Scanner;

class MatrixSum{

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n=4;
		// System.out.print("Enter size of Matrix:");
		// n = sc.nextInt();
		int m=n;
		int a[][] = new int[n][m];

		System.out.println("Enter first Array Elements("+n+"x"+m+"):");

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = sc.nextInt();
			}
		}
		int coltotal=0;
		int rowtotal=0;
		for (int i=0;i<a.length;i++) {
			coltotal=0;
			rowtotal=0;
			for (int j=0;j<a[i].length;j++) {
				rowtotal += a[i][j];
				coltotal += a[j][i];
			}
			System.out.println((i+1)+" Row Total = "+rowtotal);
			System.out.println((i+1)+" Column Total = "+coltotal);
		}

	}
}