class MyPoint{
	
	private double x,y;

	double getx(){
		return x;
	}

	double gety(){
		return y;
	}

	MyPoint(){
		this.x=0;
		this.y=0;
	}

	MyPoint(double x,double y){
		this.x=x;
		this.y=y;
	}

	MyPoint distance(MyPoint p){
		MyPoint dist = new MyPoint(Math.abs(this.x-p.x),Math.abs(this.y-p.y));
		return dist;
	}

	MyPoint distance(double x,double y){
		MyPoint dist = new MyPoint(Math.abs(this.x-x),Math.abs(this.y-y));
		return dist;
	}
}

class ThreeDPoint extends MyPoint{
	
	private double z;

	double getz(){
		return z;
	}

	ThreeDPoint(){
		super();
		this.z=0;
	}

	ThreeDPoint(double x,double y,double z){
		super(x,y);
		this.z=z;
	}

	ThreeDPoint distance(ThreeDPoint p){
		ThreeDPoint dist = new ThreeDPoint(Math.abs(super.getx()-p.getx()),Math.abs(super.gety()-p.gety()),Math.abs(this.z-p.getz()));
		return dist;
	}

}

class ThreeDPointDemo{
	public static void main(String[] args) {
		ThreeDPoint a = new ThreeDPoint(0,0,0);
		ThreeDPoint b = new ThreeDPoint(10,30,25.5);

		ThreeDPoint dist = a.distance(b);

		System.out.print("Distance => "+"("+dist.getx()+","+dist.gety()+","+dist.getz()+")");
	}
}