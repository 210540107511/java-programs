import java.util.Scanner;

class Member{
	String name;
	int age;
	long phone_no;
	String address;
	int salary;

	Member(String name,int age,long phone_no,String address,int salary){
		this.name=name;
		this.age=age;
		this.phone_no=phone_no;
		this.address=address;
		this.salary=salary;
	}

	void printSalary(){
		System.out.println("name: "+name);
		System.out.println("age: "+age);
		System.out.println("phone_no: "+phone_no);
		System.out.println("address: "+address);
		System.out.println("salary: "+salary);
	}
}

class Employee extends Member{
	String specialization;

	Employee(String name,int age,long phone_no,String address,int salary,String specialization){
		super(name,age,phone_no,address,salary);
		this.specialization=specialization;
	}

	void printEmployeeDetails(){
		System.out.println("Employee Details");
		printSalary();
		System.out.println("specialization: "+specialization+"\n");
	}
}

class Manager extends Member{
	String department;

	Manager(String name,int age,long phone_no,String address,int salary,String department){
		super(name,age,phone_no,address,salary);
		this.department=department;
	}

	void printManagerDetails(){
		System.out.println("Manager Details");
		printSalary();
		System.out.println("department: "+department+"\n");
	}
}

class EmployeeManagerDemo{
	public static void main(String[] args) {
		Employee e1 = new Employee("Brijesh",19,123456789,"rajkot",20000,"Java");
		e1.printEmployeeDetails();
		Manager m1 = new Manager("abc",28,987654321,"jetpur",50000,"CE");
		m1.printManagerDetails();
	}
}