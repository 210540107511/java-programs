import java.io.*;

class Palindrome{

	static String reverse(String str){
		String rev="";
		for (int i = str.length()-1;i>=0;i--) {
			rev+=str.charAt(i);
		}
		return rev;
	}

	public static void main(String[] args) {
		System.out.print("Enter String:");
		String str = new java.util.Scanner(System.in).next();

		if(str.equals(reverse(str))){
			System.out.print("Entered String is palindrome.");
		}else{
			System.out.print("Entered String is not palindrome.");
		}

	}

}