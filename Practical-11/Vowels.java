import java.util.*;

class Vowels{
	public static void main(String[] args) {
		char[] vowels = {'a','e','i','o','u'};

		System.out.print("Enter String:");
		String str = new java.util.Scanner(System.in).next();

		ArrayList<Character> arrayList = new ArrayList<>();

		for(char c:str.toCharArray()){
			for (char v:vowels) {
				if(c==v)
					arrayList.add(c);
			}
		}

		Set<Character> set = new LinkedHashSet<>(arrayList);

		System.out.println("vowels in "+str+" : ");
		for (char c: set) {
			System.out.print(c);
		}
	}
}