import java.util.*;
import java.util.stream.IntStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Demo1 /*extends Object*/{

/*	public static void printNum(int a){
		if(a<=100){
			System.out.println(a);
			a++;
			printNum(a);
		}
	}*/

	static String reverse(String str){
		String rev = new String();
		for (int i = str.length()-1;i>=0;i--) {
			rev+=str.charAt(i);
		}
		return rev;
	}

	void print_prime(int start,int end){
		if (end>0) {
			System.out.print("1\t");
		}
		boolean prime=true;
		for (int i=2;i<=end ;i++ ) {
			prime=true;
			for (int j=2;j<i;j++ ) {
				if(i%j==0)
					prime=false;
			}
			if(prime)
				System.out.print(i+"\t");
		}
	}

	public static void main(String[] args) {

		// IntStream.range(1,101).forEach( e -> System.out.println(e));

		// Demo1 d1 = new Demo1();

		// d1.print_prime(0,10);

/*		Integer a = 126;
		Integer b = 126;
		// a++;
		// b++;
		if (a.equals(b)) {
			System.out.print("yes");
		}else{
			System.out.print("no");
		}*/

		// String name="abcd"+'+';
		// int i;
		// for (i=0;name.charAt(i)!='+';i++ );

		// System.out.print(i);

		//01 octal
		//1 dec
		//0xF hexa
		// int a= 010;//octal
		// int b=5;
		// System.out.print("value of a+b = "+(a+b));
		// Map<Integer,Integer> map = new HashMap<>();
  //       map.put(10,2);
  //       map.put(10,0);
        
  //       map.put(20,1);

  //       System.out.print(map.get(10));
  //       System.out.print(map.get(10));
  //       System.out.print(map.get(10));
  //       System.out.print(map.get(10));

		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		String rev = reverse(str);
		
		System.out.print("rev = "+rev);
		if(str.intern() == rev.intern()) {
			System.out.println("Equals");
		}else {
			System.out.println("Not Equals");
		}
		
		// "==" tests for reference equality (whether they are the same object).

		// ".equals()" tests for value equality (whether they are logically "equal").

		// Object a = 10;
 		// System.out.println(a.toString());
        
	}

}
