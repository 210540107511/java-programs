// { Driver Code Starts
//Initial Template for Java

import java.util.*;
import java.io.*;
import java.lang.*;

class GFG
{
    public static void main(String args[])throws IOException
    {
        FileInputStream fis = new FileInputStream(new File("C:\\Users\\jagad\\Downloads\\fileInput.txt"));
        BufferedReader br = new BufferedReader(new InputStreamReader(fis)); 
        PrintWriter out=new PrintWriter(System.out);
        //taking testcases
        int t = Integer.parseInt(br.readLine()); 
        for(int i=0;i<t;i++)
        {
            String str=br.readLine();
            String[] starr=str.split(" ");
            Scanner sc = new Scanner(System.in);
            //input n and d
            int n=sc.nextInt();
            int d=sc.nextInt();
            
            int[] arr=new int[n];
            String str1=br.readLine();
            String[] starr1=str1.split(" ");
            
            //inserting elements in the array
            for(int j=0;j<n;j++)
            {
              arr[j]= Integer.parseInt(starr1[j]);
            }
            
            //calling rotateArr() function
            new Solution().rotateArr(arr, d, n);
            StringBuffer sb=new StringBuffer();
            
            //printing the elements of the array
            for(int t1=0;t1<n;t1++)
                sb.append(arr[t1]+" ");
            out.println(sb);
         }
         out.close();
    }
}
// } Driver Code Ends


//User function Template for Java
class Solution
{
    //Function to rotate an array by d elements in counter-clockwise direction. 
    static void rotateArr(int arr[], int d, int n)
    {
        int temp[] = new int[d];
        
        System.out.println(arr.length + " "+d);
        
        for(int i=0;i<d;i++){
            temp[i] = arr[i];
        }
        
        for(int i=d;i<n;i++){
            arr[i-d] = arr[i];
        }
        // System.out.println(Arrays.toString(temp));
        int c=0;
        for(int i=(n-d);i<n;i++,c++){
            arr[i]=temp[c];
        }
        
    }
}