import java.util.Scanner;

class Candidate{
	int Candidate_ID,Candidate_Age;
	String Candidate_Name;
	double Candidate_Weight,Candidate_Height;

	void GetCandidateDetails(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Candidate Details:");
		System.out.print("ID: ");
		Candidate_ID=sc.nextInt();
		System.out.print("Name: ");
		Candidate_Name=sc.next();
		System.out.print("Age: ");
		Candidate_Age=sc.nextInt();
		System.out.print("Weight: ");
		Candidate_Weight=sc.nextDouble();
		System.out.print("Height: ");
		Candidate_Height=sc.nextDouble();
	}

	void DisplayCandidateDetails(){
		System.out.println("Enter Candidate Details:");
		System.out.println("ID: "+Candidate_ID);
		System.out.println("Name: "+Candidate_Name);
		System.out.println("Age: "+Candidate_Age);
		System.out.println("Weight: "+Candidate_Weight);
		System.out.println("Height: "+Candidate_Height);
	}
}

class CandidateDemo{
	public static void main(String[] args) {
		Candidate c1 = new Candidate();
		c1.GetCandidateDetails();
		c1.DisplayCandidateDetails();
	}
}