import java.util.Scanner;

class Student{

	int Enrollment_No,Semester;
	String Student_Name;
	double CPI,SPI;

	void GetStudentDetails(){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Student Details:");
		System.out.print("Enrollment No.:");
		Enrollment_No = sc.nextInt();
		System.out.print("Student Name:");
		Student_Name = sc.next();
		System.out.print("Semester:");
		Semester = sc.nextInt();
		System.out.print("CPI:");
		CPI = sc.nextDouble();
		System.out.print("SPI:");
		SPI = sc.nextDouble();
	}

	void DisplayStudentDetails(){
		System.out.println("\nStudent Details:");
		System.out.println("Enrollment No.:"+Enrollment_No);
		System.out.println("Student Name:"+Student_Name);
		System.out.println("Semester:"+Semester);
		System.out.println("CPI:"+CPI);
		System.out.println("SPI:"+SPI);
	}
}

class StudentDemo{
	public static void main(String[] args) {
		Student s1 = new Student();
		s1.GetStudentDetails();
		s1.DisplayStudentDetails();
	}
}