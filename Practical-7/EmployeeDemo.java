import java.util.Scanner;

class Employee{
	int Employee_ID,Age,Salary;
	String Employee_Name,Designation;

	void GetEmployeeDetails(){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Employee Details:");
		System.out.print("ID:");
		Employee_ID = sc.nextInt();
		System.out.print("Name:");
		Employee_Name = sc.next();
		System.out.print("Designation:");
		Designation = sc.next();
		System.out.print("Age:");
		Age = sc.nextInt();
		System.out.print("Salary:");
		Salary = sc.nextInt();
	}

	void DisplayEmployeeDetails(){
		System.out.println("\nEmployee Details:");
		System.out.println("ID:"+Employee_ID);
		System.out.println("Name:"+Employee_Name);
		System.out.println("Designation:"+Designation);
		System.out.println("Age:"+Age);
		System.out.println("Salary:"+Salary);
	}
}

class EmployeeDemo{
	public static void main(String[] args) {
		Employee e1 = new Employee();
		e1.GetEmployeeDetails();
		e1.DisplayEmployeeDetails();
	}
}