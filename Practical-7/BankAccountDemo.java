import java.util.Scanner;

class Bank_Account{
	int Account_No,Account_Balance;
	String User_Name,Email,Account_Type;

	void GetAccountDetails(){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Account Holder's Details:");
		System.out.print("ID:");
		Account_No = sc.nextInt();
		System.out.print("Username:");
		User_Name = sc.next();
		System.out.print("Email:");
		Email = sc.next();
		System.out.print("Account Type:");
		Account_Type = sc.next();
		System.out.print("Account Balance:");
		Account_Balance = sc.nextInt();
	}

	void DisplayAccountDetails(){
		System.out.println("\nAccount Holder's Details:");
		System.out.println("ID:"+Account_No);
		System.out.println("Username:"+User_Name);
		System.out.println("Email:"+Email);
		System.out.println("Account Type:"+Account_Type);
		System.out.println("Account Balance:"+Account_Balance);
	}
}

class BankAccountDemo{
	public static void main(String[] args) {
		Bank_Account acc1 = new Bank_Account();
		acc1.GetAccountDetails();
		acc1.DisplayAccountDetails();
	}
}