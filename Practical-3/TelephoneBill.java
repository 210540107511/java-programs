/**
 * 8. WAP that prompts the user to input number of calls and calculate the monthly telephone bills as per the following rule: 
 * Minimum Rs. 200 for up to 100 calls. 
 * Plus Rs. 0.60 per call for next 50 calls. 
 * Plus Rs. 0.50 per call for next 50 calls. 
 * Plus Rs. 0.40 per call for any call beyond 200 calls.
 */

import java.util.Scanner;

public class TelephoneBill{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int calls;
		double cost=0;
		System.out.print("Enter no of calls:");
		calls = sc.nextInt();

		if(calls <= 100){
			cost += 200;
		}else{
			cost += 200;
			calls -= 100;
			if(calls <= 50 ){
				cost += calls*0.6;
			}else{
				cost += 50*0.6;
				calls -= 50;
				if(calls <= 50 ){
					cost += calls*0.5;
				}else{
					cost += 50*0.5;
					calls -= 50;
					cost += calls*0.4;
				}
			}
		}

		System.out.println("Answer: "+cost);
	}
}