/**
 * Three sides of a triangle are entered through the keyboard. WAP to check whether the triangle is isosceles, equilateral, scalene or right angled triangle.
 */q

import java.util.Scanner;
import java.lang.Math;

public class Triangle{

	private static boolean right_angled(int a,int b,int c){
		double _a=Math.pow(a,2),_b=Math.pow(b,2),_c=Math.pow(c,2);

		return (_a + _b == _c) || (_b + _c == _a) || (_a + _c == _b) ;
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int a,b,c;

		System.out.print("Enter all(3) sides of triangle:");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();


		//chacking for isosceles two sides are equal...
		
		if( a==b && b==c && c==a )
			System.out.println("equilateral");
		else if( a==b || b==c || c==a )
			System.out.println("isosceles");
		else
			System.out.println("scalene");

		//right angled triangle?

		if(right_angled(a,b,c)){
			System.out.println("right angled triangle");
		}
		

	}
}