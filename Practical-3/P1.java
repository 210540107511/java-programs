/**
 * WAP to check Whether the given number is positive or negative.
 */

import java.util.Scanner;

public class P1 {
	public static void main(String[] args) {
		
		int num;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Number:");
		num = sc.nextInt();
		
		if(num < 0) {
			System.out.println(num+" is negative.");
		}else {
			System.out.println(num+" is Positive.");
		}
	}
}
