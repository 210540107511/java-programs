/**
 * WAP to read marks of five subjects. Calculate percentage and print class accordingly.Fail below 35, Pass class 35 to 45, Second class between 45 to 60, First class between 60 to 70, Distinction if more than 70. 
 */

import java.util.Scanner;

public class CalculatePercentage {
	public static void main(String[] args) {

		int n=5;
		int subjects[] = new int[5];
		int total=0,percentage;
		
		Scanner sc = new Scanner(System.in);
	
		System.out.print("Enter Marks of 5 subjects:");
		for (int i=0;i<n;i++) {
			subjects[i]=sc.nextInt();
			total+=subjects[i];
		}
		percentage = total/n;

		System.out.println("You have scored "+percentage+"%.\n");

		if(percentage<35){
			System.out.println("you were unable to pass the exam.[Failed]");
		}else if(percentage<=45){
			System.out.println("you have sucessfully passed the exam.");
		}else if(percentage<=60){
			System.out.println("you have passed the exam with Second Class");
		}else if(percentage<=70){
			System.out.println("you have passed the exam with First Class");
		}else {
			System.out.println("you have passed the exam with distinction");
		}

	}
}
