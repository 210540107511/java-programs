/**
 * WAP that prompts the user to enter a letter and check Whether a letter is a vowel or consonants. 
 */

import java.util.Scanner;

public class P2 {
	public static void main(String[] args) {
		char ch;
		char vowels[]= {'a','e','i','o','u','A','E','I','O','U'};
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter character:");
		ch = sc.next().charAt(0);
		int flag = 0;
		for(int i=0;i<10;i++) {
			if(ch == vowels[i]) {
				flag = 1;
				break;
			}
		}
		if(flag == 1) {
			System.out.println("given character is Vowel");
		}else {
			System.out.println("given character is Consonant");
		}
		
	}
}
