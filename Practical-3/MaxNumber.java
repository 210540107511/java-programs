/**
 * WAP to find out largest number from 3 numbers using conditional operator
 */

import java.util.Scanner;

public class MaxNumber{
	public static void main(String[] args) {
		
		int a,b,c;
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter 3 numbers:");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();

		int ans = (a>b)?((a>c)?a:c):((b>c)?b:c);

		System.out.println("Max number out of those 3 number is:"+ans);
	}
}