/**
 * WAP to find out largest number from given three numbers without using logical operator.
 */

import java.util.Scanner;

public class P3 {
	static int max(int a,int b,int c) {
		if(a>=b){
			if(a>=c) {
				return a;
			}else {
				return c;
			}
		}else {
			if(b>=c){
				return b;
			}else {
				return c;
			}
		}
	}
	public static void main(String[] args) {
		int a,b,c;
		int maximum;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter a:");
		a=sc.nextInt();
		System.out.println("Enter b:");
		b=sc.nextInt();
		System.out.println("Enter c:");
		c=sc.nextInt();
		
		maximum = max(a,b,c);
//		maximum =  a > b ? (a > c ? a : c) : (b > c ? b : c);
		
		
		System.out.println("Maximum number is: "+maximum);
		
	}
}
