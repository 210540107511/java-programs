/**
 * WAP to make a Simple Calculator using switch...case.
 */

import java.text.DecimalFormat;
import java.util.Scanner;

public class Calculator{

	private static final DecimalFormat df = new DecimalFormat("0.0000");

	public static void main(String[] args) {

		int a,b;
		double ans=0;
		Scanner sc = new Scanner(System.in);
		char op;
		do{

			System.out.println("Enter First Number:");
			a = sc.nextInt();
			System.out.println("Enter Second Number:");
			b = sc.nextInt();

			System.out.println("Enter operation to be performed:");
			op = sc.next().charAt(0);

			switch(op)
			{
				case '+': ans = a+b;
						  break;
				case '-': ans = a-b;
						  break;
				case '*': ans = a*b;
						  break;
				case '/': 
						  ans =  Double.parseDouble(df.format((double)a/b));
						  break;
				default:
						  System.exit(0);
			}

			System.out.println("Answer: "+ans);
		}while(true);
	}
}