import java.util.Scanner;

class Maximum{

	private static int max(int a,int b){
		return (a>b)?a:b;
	}

	public static void main(String[] args) {
		int a,b;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter 2 numbers:");
		a = sc.nextInt();
		b = sc.nextInt();

		System.out.print("Maximum is: "+max(a,b));
	}
}