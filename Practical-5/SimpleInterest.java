import java.util.Scanner;

class SimpleInterest{

	private static double SI(int P,double R,int N){
		return (P*R*N/100);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int p,n;
		double r;

		System.out.print("Enter Principal Amount:");
		p = sc.nextInt();
		System.out.print("Enter Rate:");
		r = sc.nextDouble();
		System.out.print("Enter Time:");
		n = sc.nextInt();

		double amount = SI(p,r,n);
		
		System.out.print("Simple Interest : "+amount);
	}
}