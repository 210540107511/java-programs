import java.util.Scanner;

class Fibonacci{

	private static void fibonacci(int n){
		
		int i=1,j=2,temp;
		if(n>2){
			System.out.print(" 0 1 ");
			n-=2;
			while(n>0){
				System.out.print(" "+i+" ");
				temp = i;
				i=j;
				j+=temp;
				n--;
			}
		}else if(n==2){
			System.out.print(" 0 1 ");
		}else if(n==1){
			System.out.print(" 0 ");
		}else{
			//
		}
		
	}

	public static void main(String[] args) {
		System.out.print("Enter Number:");
		int n = new Scanner(System.in).nextInt();
		fibonacci(n);
	}
}