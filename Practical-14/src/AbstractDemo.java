/**
 * 
 * @author Brijesh Jagad
 *
 */

abstract class Vegetable{
	String color;
}

class Potato extends Vegetable{
	Potato(String color){
		this.color = color;
	}
	@Override
	public String toString() {
		
		return "It is a Potato and it is "+color+".";
	}
	
}

class Brinjal extends Vegetable{
	Brinjal(String color){
		this.color = color;
	}
	
	@Override
	public String toString() {
		
		return "It is a Brinjal and it is "+color+".";
	}
}

class Tomato extends Vegetable{
	Tomato(String color){
		this.color = color;
	}
	
	@Override
	public String toString() {
		
		return "It is a Tomato and it is "+color+".";
	}
}

public class AbstractDemo {
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		
		Potato p = new Potato("skin");
		System.out.println(p);
		
		Brinjal b = new Brinjal("purple");
		System.out.println(b);
		
		Tomato t = new Tomato("red");
		System.out.println(t);
	
	}
}
