/**
 * 3. WAP to print your address i) using single print ii) using multiple println
 */

public class P3 {
    public static void main(String[] args) {
        System.out.print("Patel Nagar,\nSardar Chowk,\nJetpur - 360370\n");

        System.out.println("Patel Nagar,");
        System.out.println("Sardar Chowk,");
        System.out.println("Jetpur - 360370");
    }
}