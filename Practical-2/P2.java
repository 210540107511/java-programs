/**
 * 2. WAP to calculate Area of Circle.
 */

import java.util.Scanner;

public class P2 {
    public static void main(String[] args) {

        int r;
        double pi = 3.14;
        double area;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter Radius:");
        r = sc.nextInt();

        area = pi * r * r;

        System.out.println("Area of Circle:"+area);
    }
}
