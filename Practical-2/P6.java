/**
 * Body Mass Index (BMI) is a measure of health on weight. 
 * It can be calculated by taking your weight in kilograms and 
 * dividing by the square of your height in meters. 
 * Write a program that prompts the user to enter a weight in pounds and 
 * height in inches and displays the BMI. 
 * Note:- 1 pound = .45359237 Kg and 1 inch = .0254 meters.
 */

import java.util.Scanner;

public class P6 {
	public static void main(String[] args) {
		float weight_pounds,height_inches,BMI;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Weight (in pounds):");
		weight_pounds = sc.nextFloat();
		
		System.out.println("Enter Height (in inches):");
		height_inches = sc.nextFloat();
		
		BMI = (weight_pounds * 0.45359237f) / ((height_inches*0.0254f)*(height_inches*0.0254f)); 
		
		System.out.println("Your BMI is: "+BMI);
	}
}
