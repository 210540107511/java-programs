/**
 * WAP to find percentage of 5 subjects.
 */

import java.util.Scanner;

public class P4 {
	public static void main(String[] args) {
		int subjects[] = new int[5];
		int sum=0;
		float percentage;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Marks of all subjects (5):");
		for(int i=0;i<5;i++) {
			subjects[i] = sc.nextInt(); 
			sum+=subjects[i];
		}
		percentage = (float)sum/5;
		System.out.println("Percentage :"+percentage+"%");
	}
}
