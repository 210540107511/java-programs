/**
 * 1. WAP to print addition of 2 numbers (with Scanner)
 */

import java.util.Scanner;

public class P1 {

    public static void main(String[] args) {

        int a,b,c;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter A:");
        a = sc.nextInt();

        System.out.print("Enter B:");
        b = sc.nextInt();

        c = a + b;

        System.out.print("Sum of both numbers:"+c);
    }
}
