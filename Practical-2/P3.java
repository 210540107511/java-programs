/**
 * 3. WAP to convert temperature form Fahrenheit to Celsius.
 */

import java.util.Scanner;

public class P3 {
    public static void main(String[] args) {

        float fahrenheit,celsius;
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter temperature in fahrenheit:");
        fahrenheit = sc.nextFloat();

        celsius = (fahrenheit - 32) * ((float) 5/9);

        System.out.println(fahrenheit+" degree Fahrenheit is equal to "+celsius+" degree Celsius.");
    }
}
