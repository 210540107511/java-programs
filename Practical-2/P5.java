/**
 * WAP that reads a number in meters, convert it to feet, and displays the result.
 */

import java.util.Scanner;

public class P5 {
	
	public static void main(String[] args) {
		float meters,feet;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter number (in meters):");
		meters = sc.nextFloat();
		
		feet = meters*3.28084f;
		
		System.out.println(meters +" meters in feet is "+feet+".");
	}
	
}
