import java.util.Scanner;

class Student_Details{
	int Enrollment_No,Semester;
	String Student_Name;
	double CPI;

	void GetStudentDetails(int Enrollment_No,String Student_Name,int Semester,double CPI){
		this.Enrollment_No=Enrollment_No;
		this.Student_Name=Student_Name;
		this.Semester=Semester;
		this.CPI=CPI;
	}

	void DisplayStudentDetails(){
		
		System.out.println("Enrollment No.: "+Enrollment_No);
		System.out.println("Name: "+Student_Name);
		System.out.println("Semester: "+Semester);
		System.out.println("CPI: "+CPI+"\n");
	}
}

class StudentArrayDemo{
	public static void main(String[] args) {

		Student_Details[] stu = new Student_Details[2];
		Scanner sc = new Scanner(System.in);
		int Enrollment_No,Semester;
		String Student_Name;
		double CPI;

		for (int i=0;i<stu.length;i++) {
			stu[i] = new Student_Details();
			System.out.println("\nEnter Student "+(i+1)+" Details:");
			System.out.print("Enrollment No.:");
			Enrollment_No=sc.nextInt();
			System.out.print("Name:");
			Student_Name=sc.next();
			System.out.print("Semester:");
			Semester=sc.nextInt();
			System.out.print("CPI:");
			CPI=sc.nextDouble();
			stu[i].GetStudentDetails(Enrollment_No,Student_Name,Semester,CPI);
		}

		for (int i=0;i<stu.length;i++ ) {
			System.out.println("\nStudent Details "+(i+1)+" :");
			stu[i].DisplayStudentDetails();
		}
	}
}