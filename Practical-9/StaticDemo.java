class Student{
	static int count=0;
	Student(){
		count++;
	}

	static void displayCount(){
		System.out.println("Count = "+count);
	}
}

class StaticDemo{
	public static void main(String[] args) {
		Student s1=new Student();
		Student.displayCount();
		Student s2=new Student();
		Student s3=new Student();
		Student s4=new Student();
		Student s5=new Student();
		Student.displayCount();
	}
}