import java.util.Scanner;

class Student{
	int Enrollment_No,Semester;
	String Student_Name;

	void GetStudentDetails(int Enrollment_No,String Student_Name,int Semester){
		this.Enrollment_No=Enrollment_No;
		this.Student_Name=Student_Name;
		this.Semester=Semester;
	}
}

class Result {
	int Enrollment_No;
	double CPI,SPI;

	void GetResultDetails(int Enrollment_No,double CPI,double SPI){

		this.Enrollment_No=Enrollment_No;
		this.CPI=CPI;
		this.SPI=SPI;
	}

	void DisplayResult(Student s){
		if(this.Enrollment_No == s.Enrollment_No){
			System.out.println("Student Result Details:");
			System.out.println("Enrollment No.: "+s.Enrollment_No);
			System.out.println("Name: "+s.Student_Name);
			System.out.println("Semester: "+s.Semester);
			System.out.println("CPI: "+CPI);
			System.out.println("SPI: "+SPI);
		}else{
			System.out.print("Student Details Not Found...");
		}

	}
}

class StudentDemo{
	public static void main(String[] args) {
		Result r1 = new Result();
		Student s1 = new Student();
		s1.GetStudentDetails(153,"Brijesh",4);
		r1.GetResultDetails(153,8.6,9.22);
		r1.DisplayResult(s1);
	}	
}