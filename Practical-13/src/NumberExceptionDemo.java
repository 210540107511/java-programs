import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * 
 * @author Brijesh Jagad
 *
 */
public class NumberExceptionDemo {
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	
		try {
			int a = Integer.parseInt(args[0]); 
			String operator = args[1];
			int b = Integer.parseInt(args[2]);
			System.out.println("Valid Expression");
			
		}catch(NumberFormatException nfe) {
			System.out.println("Please Enter Valid Numbers.");
		}

	}

}
