/**
 * WAP to find whether a number is prime or not.
 */

import java.util.Scanner;

class Prime{
	public static void main(String[] args) {

		boolean prime=true;

		System.out.print("Enter Number:");
		int n = new Scanner(System.in).nextInt();

		if(n==0 || n==1){
			System.out.print("Number is prime.");
		}else{
			for (int i=2;i<=n/2;i++) {
				if(n%i==0){
					prime=false;
					break;
				}
			}
			if (prime) {
				System.out.println("Number is prime.");
			}else{
				System.out.println("Number is non-prime.");
			}
		}
		
	}
}
