
import java.util.Scanner;

class Reverse{
	public static void main(String[] args) {
		
		int rev=0;

		System.out.print("Enter Number:");
		int n = new Scanner(System.in).nextInt();

		while(n>0){
			rev *= 10;
			rev += (n%10);
			n /= 10;
		}
		
		System.out.print("Reverse Number is : "+rev);

	}
}