import java.util.Scanner;
class Divide{
	public static void main(String[] args) {
		System.out.print("Enter number to Start from");
		int a = new Scanner(System.in).nextInt();
		System.out.print("Enter number to End with");
		int b = new Scanner(System.in).nextInt();
		
		System.out.println("The numbers are:");
		for (int i=a;i<=b ;i++ ) {
			if(i%2==0 && i%3==0)
				System.out.println(i);
		}
	}
}