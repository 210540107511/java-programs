import java.util.Scanner;

class SumNumbers{
	public static void main(String[] args) {
		int n,even=0,odd=0;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter numbers [0 -> Exit]:");
		n = sc.nextInt();
		while(n!=0){
			if (n>0 && n%2==0) {
				even += n;
			}
			if(n<0 && n%2!=0){
				odd+=n;
			}
			n = sc.nextInt();
		}

		System.out.println("Sum of all positive even numbers: "+even);
		System.out.println("Sum of all negative odd numbers: "+odd);

	}
}