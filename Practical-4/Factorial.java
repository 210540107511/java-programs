
import java.util.Scanner;

class Factorial{
	public static void main(String[] args) {
		System.out.print("Enter number:");
		int a = new Scanner(System.in).nextInt();
		int fact=1;
		for (int i=1;i <= a;i++) {
			fact*=i;
		}
		System.out.print("Factorial of given number is:"+fact);
		
	}
}