import java.util.Scanner;

class Complex{
	int real;
	int imaginary;

	Complex(int real){
		this.real = real;
		this.imaginary = 0;
	}

	Complex(int real,int imaginary){
		this.real = real;
		this.imaginary = imaginary;
	}

	void display(){
		// System.out.println("Number : "+ (real==0?"":real) + (imaginary==0?"":" + " +imaginary+"i") );
		System.out.println("Number : " + real + " + " + imaginary + "i");
	}

	static Complex add(Complex c1,Complex c2){

		Complex c = new Complex(0,0);

		c.real = c1.real + c2.real;
		c.imaginary = c1.imaginary + c2.imaginary;

		return c;
	}
}

class ComplexDemo{
	public static void main(String[] args) {
		Complex c1 = new Complex(10,5);
		c1.display();
		Complex c2 = new Complex(10,10);
		c2.display();

		Complex c3 = Complex.add(c1,c2);
		c3.display();
	}
}