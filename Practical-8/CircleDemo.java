import java.util.Scanner;

class Circle{
	private static final double PI = 3.14;

	double area(double r){
		return PI*r*r;
	}

	double perimeter(double r){
		// System.out.print(PI);
		return PI*r;
	}

}

class CircleDemo{
	public static void main(String[] args) {
		Circle c1 = new Circle();

		System.out.print("Enter Radius: ");
		double radius = new Scanner(System.in).nextDouble();

		double area = c1.area(radius);
		double perimeter = c1.perimeter(radius);

		System.out.println("Area of Circle:"+area);
		
		String s=String.format("%.2f",perimeter);
		System.out.println("Perimeter of Circle: "+s);

	}
}