import java.util.Scanner;

class Time{
	int hour;
	int minute;

	Time(int hour,int minute){

		this.hour = hour;
		this.minute = minute;

	}

	static Time add(Time t1,Time t2){
		Time t = new Time(0,0);
		t.hour = t1.hour + t2.hour;
		t.minute = t1.minute + t2.minute;
		t.hour += t.minute/60;
		t.minute %= 60;
		return t;	
	}

}

class TimeDemo{

	public static void main(String[] args) {
		
		Time t1 = new Time(10,30);
		Time t2 = new Time(7,50);

		Time t = Time.add(t1,t2);

		System.out.print("Time = "+t.hour+":"+t.minute);
	}

}